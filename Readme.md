This repo contains an example of a Github app. A Github app is a way of adding more features to Github.

## Probot
Probot is a set of open source and free hosted GitHub apps.
- This is the main page [https://probot.github.io/](https://probot.github.io/)
- Here you can find all the available apps [https://probot.github.io/apps/](https://probot.github.io/apps/)
  - You can add it to all your repos or some of them