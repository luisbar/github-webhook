const express = require('express');
const { checkDependenciesVersion } = require('./checkStatuses/checkDependenciesVersion');

const app = express();
app.use(express.json());

app.post('/events', async (req, res) => {
  try {
    if (req.body.action !== 'opened' && req.body.action !== 'reopened') return res.json({});
    console.log('Check statuses processing');
    const installationId = req.body.installation.id;
    const repoName = req.body.repository.full_name;
    const sha = req.body.pull_request.head.sha;
    res.json(await checkDependenciesVersion(installationId, repoName, sha));
    console.log('Check statuses done');
  } catch (error) {
    console.log(error.message);
  }
});

app.listen(3001, () => console.log('Github webhook listening on port 3001!'));