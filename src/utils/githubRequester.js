const axios = require('axios');
const { createJWT } = require('./createJWT');

exports.githubRequester = async (installationId, { path, headers, ...others }) => {
  const token = await createJWT(installationId); // TO DO: save token and check if it is still valid
  const response = await axios({
    url: `https://api.github.com${path}`,
    headers: {
      authorization: `Bearer ${token}`,
      accept: 'application/vnd.github.machine-man-preview+json',
      ...headers,
    },
    ...others,
  });

  return response.data;
}
