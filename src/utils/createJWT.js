const { createAppAuth } = require('@octokit/auth-app');
const fs = require('fs');
const privatekey = fs. readFileSync(`${process.cwd()}/private-key.pem`, 'utf8') ;

exports.createJWT = async (installationId) => {
  const authenticate = createAppAuth({
    appId: process.env.GITHUB_APP_ID,
    privateKey: privatekey,
    installationId,
    clientId: process.env.GITHUB_APP_CLIENT_ID,
    clientSecret: process.env.GITHUB_APP_CLIENT_SECRET,
  });
  const { token } = await authenticate({ type: 'installation' });
  return token;
};