exports.isVersioningCorrect = (dependencies) => {
  return !Object.keys(dependencies).find(key => {
    return !/^\d+\.\d+\.\d+$/.test(dependencies[key])
  });
}
