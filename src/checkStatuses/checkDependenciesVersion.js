const { githubRequester } = require('../utils/githubRequester');
const { isVersioningCorrect } = require('../utils/isVersioningCorrect');

exports.checkDependenciesVersion = async (installationId, repo, sha) => {
  let { dependencies } = await githubRequester(installationId, {
    method: 'GET',
    path: `/repos/${repo}/contents/package.json`,
  })
  .then(res => res.content)
  .then(content => JSON.parse(Buffer.from(content, 'base64'). toString('utf8')));

  const ok = isVersioningCorrect(dependencies);
  
  await githubRequester(installationId, {
    path: `/repos/${repo}/check-runs`,
    method: 'POST',
    headers: {
      accept: 'application/vnd.github.antiope-preview+json',
    },
    data: {
      name: 'strict-dependencies',
      head_sha: sha,
      status: 'completed',
      conclusion: ok ? 'success' : 'failure',
      output: {
        title: ok
        ? 'No semver ranges found'
        : 'Semver ranges found!',
        summary: ok
        ? 'Good job!'
        : 'Found a semver range in dependencies',
      },
    },
  });
};